# MONTHLY CONSUMPTION CALCULATION

## Description

contains an application implementing :

- a service that calculates the amount of the monthly consumption for EKWATEUR customers.

## Technical Dependencies

- Java 21
- Maven 3.9.x (it was developed using Maven 3.9.3)
- SpringBoot 3
- Docker
- PostgreSQL

## Before starting the application

### SdkMan autoconfiguration

(for developers using [SDKMAN](https://sdkman.io/))

In file $HOME/.sdkman/etc/config you must add or modify the following line sdkman_auto_env=true. With this option we can
simply change to our project directory and the correct Java version is set to active.

### Docker configuration

Install [Rancher Desktop](https://rancherdesktop.io/).

<!-- TODO : do it for Windows and Linux -->

On **MacOS**, on the Welcome screen, don't enable Kubernetes.
On the Preferences screen > Application > General, check "Administrative Access"

Once Rancher Desktop is up and running, run postgres on a Docker container :

``` bash 
docker compose -f docker/docker-compose.yaml up -d
```

### Generated classes

In order to generate the classes defined by the [Interface Contract](openapi.yaml) (via
Swagger-codegen-maven-plugin) and created by Mapstruct, just run

``` bash 
mvn compile
```

Add *target/generated-sources/annotations* and *target/generated-sources/swagger/src/main/java* to your classpath.

## The application

### Starting the application

``` bash
mvn spring-boot:run
```

### Actuator

It can be found [here](http://localhost:8080/actuator)

### Swagger

[Here](http://localhost:8080/swagger-ui/index.html) it is !

### API Documentation

It can be found [here](http://localhost:8080/api-docs)

### The endpoint

The endpoint is '/customers/{account-id}/{year-month}/monthly-consumption' (GET monthly consumption)
deposit distribution)

## While developing this application

### Automatic format check

In order to follow some common rules regarding code formatting, this project uses Spotless.

You can use it manually by running

``` bash
mvn spotless:check
```

to check for formatting issues, and by running

``` bash
mvn spotless:apply
```

Configure your IDE so it doesn't use wildcards for imports.

To apply Spotless' formatting rules.

For automatising the formatting check before each commit, in the project's root folder, run

``` bash
git config core.hooksPath .githooks
```

Make it executable :

``` bash
chmod +x .githooks/pre-commit
```

/!\ Do not forget to rerun 'git add' and 'git commit' when you have to do a 'mvn spotless:apply' after a **BUILD FAILURE
** due to a 'mvn spotless:check'

### Test coverage

This project requires an instruction coverage minimum of 80%.
You can run a coverage check by running

``` bash 
mvn verify
```

Then, the code coverage report (Jacoco) can be found [here](/target/site/jacoco/index.html)

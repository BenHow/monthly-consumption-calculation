package com.ekwateur.monthlyconsumption;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonthlyConsumptionCalculatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(MonthlyConsumptionCalculatorApplication.class, args);
    }

}

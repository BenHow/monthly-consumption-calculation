package com.ekwateur.monthlyconsumption.database.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "customers")
public class Customer {

    @Id
    @Column(unique = true, nullable = false)
    private String accountId;

    @Enumerated(EnumType.STRING)
    private EnergyType energyType;

    private BigDecimal yearlyQuantityKwh;

    private String firstName;

    private String lastName;

    @Enumerated(EnumType.STRING)
    private Title title;

    private Long directoryId;

    private String companyName;

    private Long revenue;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public EnergyType getEnergyType() {
        return energyType;
    }

    public void setEnergyType(EnergyType energyType) {
        this.energyType = energyType;
    }

    public BigDecimal getYearlyQuantityKwh() {
        return yearlyQuantityKwh;
    }

    public void setYearlyQuantityKwh(BigDecimal yearlyQuantityKwh) {
        this.yearlyQuantityKwh = yearlyQuantityKwh;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public Long getDirectoryId() {
        return directoryId;
    }

    public void setDirectoryId(Long directoryId) {
        this.directoryId = directoryId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Long getRevenue() {
        return revenue;
    }

    public void setRevenue(Long revenue) {
        this.revenue = revenue;
    }
}

package com.ekwateur.monthlyconsumption.database.model;

public enum EnergyType {

    GAS, ELECTRICITY
}

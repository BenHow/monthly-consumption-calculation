package com.ekwateur.monthlyconsumption.database.model;

public enum Title {

    M("Monsieur"), MME("Madame"), OTHER("Autre");

    public final String label;

    Title(String label) {
        this.label = label;
    }
}

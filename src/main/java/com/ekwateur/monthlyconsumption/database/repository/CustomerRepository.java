package com.ekwateur.monthlyconsumption.database.repository;

import com.ekwateur.monthlyconsumption.database.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, String> {
}

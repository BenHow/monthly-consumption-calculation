package com.ekwateur.monthlyconsumption.delegate;

import com.ekwateur.monthlyconsumption.mapper.MonthlyConsumptionMapper;
import com.ekwateur.monthlyconsumption.rest.api.MonthlyConsumptionApiDelegate;
import com.ekwateur.monthlyconsumption.rest.model.MonthlyConsumptionResponseDto;
import com.ekwateur.monthlyconsumption.service.MonthlyConsumptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class MonthlyConsumptionApiDelegateImpl implements MonthlyConsumptionApiDelegate {

    @Autowired
    MonthlyConsumptionService monthlyConsumptionService;

    @Autowired
    private MonthlyConsumptionMapper mapper;

    @Override
    public ResponseEntity<MonthlyConsumptionResponseDto> getMonthlyConsumption(String accountId, String yearMonth) {
        return ResponseEntity.ok().body(mapper.toMonthlyConsumptionResponseDto(monthlyConsumptionService.calculateMonthlyConsumption(accountId, yearMonth)));

    }
}

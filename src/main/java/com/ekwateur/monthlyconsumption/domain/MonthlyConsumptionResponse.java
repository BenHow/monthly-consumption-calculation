package com.ekwateur.monthlyconsumption.domain;

public record MonthlyConsumptionResponse(String monthlyConsumption) {

}

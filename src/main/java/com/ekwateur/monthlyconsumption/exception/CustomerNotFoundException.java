package com.ekwateur.monthlyconsumption.exception;

public class CustomerNotFoundException extends RuntimeException {

    private static final String MSG = "This customer doesn't exist.";

    public CustomerNotFoundException() {
        super(MSG);

    }

}
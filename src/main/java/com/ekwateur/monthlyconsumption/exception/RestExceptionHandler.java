package com.ekwateur.monthlyconsumption.exception;

import com.ekwateur.monthlyconsumption.rest.model.ErrorDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger exceptionLogger = LoggerFactory.getLogger(RestExceptionHandler.class);

    @ExceptionHandler(value = { CustomerNotFoundException.class })
    protected ResponseEntity<Object> handleCustomerNotFoundException(CustomerNotFoundException ex, WebRequest request) {
        ErrorDto bodyOfResponse = new ErrorDto().message(ex.getMessage());
        exceptionLogger.debug("CustomerNotFoundException : ", ex);
        return handleExceptionInternal(ex, bodyOfResponse,
                new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

}

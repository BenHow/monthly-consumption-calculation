package com.ekwateur.monthlyconsumption.mapper;

import com.ekwateur.monthlyconsumption.domain.MonthlyConsumptionResponse;
import com.ekwateur.monthlyconsumption.rest.model.MonthlyConsumptionResponseDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MonthlyConsumptionMapper {
    MonthlyConsumptionResponseDto toMonthlyConsumptionResponseDto(MonthlyConsumptionResponse monthlyConsumptionResponse);
}

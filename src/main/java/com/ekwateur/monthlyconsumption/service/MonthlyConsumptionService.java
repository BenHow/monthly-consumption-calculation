package com.ekwateur.monthlyconsumption.service;

import com.ekwateur.monthlyconsumption.database.model.Customer;
import com.ekwateur.monthlyconsumption.database.model.EnergyType;
import com.ekwateur.monthlyconsumption.database.repository.CustomerRepository;
import com.ekwateur.monthlyconsumption.domain.MonthlyConsumptionResponse;
import com.ekwateur.monthlyconsumption.exception.CustomerNotFoundException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.YearMonth;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MonthlyConsumptionService {

    public static final String GAS_SME_RATE = "0.113";
    public static final String ELECTRICITY_LARGE_COMPANY_RATE = "0.114";
    public static final String GAS_LARGE_COMPANY_RATE = "0.111";
    public static final int SME_TO_LARGE_COMPANY_THRESHOLD = 1000000;
    private final static String ELECTRICITY_CONSUMER_RATE = "0.121";
    private final static String GAS_CONSUMER_RATE = "0.115";
    private final static String ELECTRICITY_SME_RATE = "0.118";
    private static final Logger logger = LoggerFactory.getLogger(MonthlyConsumptionService.class);
    @Autowired
    CustomerRepository customerRepository;

    public MonthlyConsumptionResponse calculateMonthlyConsumption(String accountId, String yearMonthString) {
        logger.info("Starting monthly consumption@ calculation for account-id : {}", accountId);
        Optional<Customer> optionalCustomer = customerRepository.findById(accountId);
        if (optionalCustomer.isPresent()) {
            Customer customer = optionalCustomer.get();
            BigDecimal rate = getRate(customer);
            // yearLyQuantityKwh is rounded to the nearest kwh
            BigDecimal yearlyAmount = rate.multiply(customer.getYearlyQuantityKwh().setScale(0, RoundingMode.HALF_UP));
            YearMonth yearMonth = YearMonth.parse(yearMonthString);
            // first multiply by the number of days in the month, then divide by the number of days in the year (to avoid rounding too soon), then round up to the nearest cent
            return new MonthlyConsumptionResponse(yearlyAmount.multiply(BigDecimal.valueOf(yearMonth.lengthOfMonth())).divide(BigDecimal.valueOf(yearMonth.lengthOfYear()), 2, RoundingMode.HALF_UP).toString());
        } else {
            throw new CustomerNotFoundException();

        }
    }

    private BigDecimal getRate(Customer customer) {
        BigDecimal rate;
        if (customer.getDirectoryId() != null) {
            if (customer.getRevenue() > SME_TO_LARGE_COMPANY_THRESHOLD) {
                rate = customer.getEnergyType().equals(EnergyType.ELECTRICITY) ? new BigDecimal(ELECTRICITY_LARGE_COMPANY_RATE) : new BigDecimal(GAS_LARGE_COMPANY_RATE);
            } else {
                rate = customer.getEnergyType().equals(EnergyType.ELECTRICITY) ? new BigDecimal(ELECTRICITY_SME_RATE) : new BigDecimal(GAS_SME_RATE);
            }
        } else {
            rate = customer.getEnergyType().equals(EnergyType.ELECTRICITY) ? new BigDecimal(ELECTRICITY_CONSUMER_RATE) : new BigDecimal(GAS_CONSUMER_RATE);
        }

        return rate;
    }
}

package com.ekwateur.monthlyconsumption.delegate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.ekwateur.monthlyconsumption.domain.MonthlyConsumptionResponse;
import com.ekwateur.monthlyconsumption.rest.api.MonthlyConsumptionApiDelegate;
import com.ekwateur.monthlyconsumption.rest.model.MonthlyConsumptionResponseDto;
import com.ekwateur.monthlyconsumption.service.MonthlyConsumptionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;

@SpringBootTest
class MonthlyConsumptionApiDelegateImplTest {

    @Autowired
    private MonthlyConsumptionApiDelegate monthlyConsumptionApiDelegate;

    @MockBean
    private MonthlyConsumptionService monthlyConsumptionService;

    @Test
    void getMonthlyConsumption_should_return_monthly_consumption() {
        // GIVEN
        MonthlyConsumptionResponse response = new MonthlyConsumptionResponse("1000");
        when(monthlyConsumptionService.calculateMonthlyConsumption(any(), any())).thenReturn(response);

        // WHEN
        ResponseEntity<MonthlyConsumptionResponseDto> monthlyConsumptionResponse = monthlyConsumptionApiDelegate.getMonthlyConsumption("EKW12345678", "2023-01");

        // THEN
        assertTrue(monthlyConsumptionResponse.getStatusCode().is2xxSuccessful());
        assertNotNull(monthlyConsumptionResponse.getBody());
        assertEquals("1000", monthlyConsumptionResponse.getBody().getMonthlyConsumption());
    }

}
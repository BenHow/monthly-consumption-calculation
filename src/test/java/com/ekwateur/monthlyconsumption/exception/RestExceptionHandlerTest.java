package com.ekwateur.monthlyconsumption.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

import com.ekwateur.monthlyconsumption.rest.model.ErrorDto;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.WebRequest;

class RestExceptionHandlerTest {

    @Test
    void testHandleCustomerNotFoundException() {
        RestExceptionHandler handler = new RestExceptionHandler();
        CustomerNotFoundException exception = new CustomerNotFoundException();
        WebRequest webRequest = mock(WebRequest.class);
        ResponseEntity<Object> response = handler.handleCustomerNotFoundException(exception, webRequest);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        ErrorDto body = (ErrorDto) response.getBody();
        assertNotNull(body);
    }

}
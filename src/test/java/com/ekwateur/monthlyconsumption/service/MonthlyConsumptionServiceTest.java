package com.ekwateur.monthlyconsumption.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.ekwateur.monthlyconsumption.database.model.Customer;
import com.ekwateur.monthlyconsumption.database.model.EnergyType;
import com.ekwateur.monthlyconsumption.database.repository.CustomerRepository;
import com.ekwateur.monthlyconsumption.domain.MonthlyConsumptionResponse;
import com.ekwateur.monthlyconsumption.exception.CustomerNotFoundException;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
class MonthlyConsumptionServiceTest {

    @Autowired
    private MonthlyConsumptionService monthlyConsumptionService;

    @MockBean
    private CustomerRepository customerRepository;

    private static Stream<Arguments> provideParametersForCustomer() {
        return Stream.of(
                Arguments.of(123L, EnergyType.GAS, 1000001L, "2023-01", "20.93"),
                Arguments.of(123L, EnergyType.ELECTRICITY, 1000001L, "2024-02", "20.05"),
                Arguments.of(123L, EnergyType.GAS, 100L, "2023-01", "21.31"),
                Arguments.of(123L, EnergyType.ELECTRICITY, 100L, "2023-01", "22.25"),
                Arguments.of(null, EnergyType.ELECTRICITY, null, "2023-01", "22.81"),
                Arguments.of(null, EnergyType.GAS, null, "2023-01", "21.68"));
    }

    @ParameterizedTest
    @MethodSource("provideParametersForCustomer")
    void should_return_MonthlyConsumptionResponse(Long directoryId, EnergyType energyType, Long revenue, String date, String expected) {
        // GIVEN
        Customer customer = new Customer();
        customer.setAccountId("EKW12345678");
        customer.setEnergyType(energyType);
        customer.setYearlyQuantityKwh(BigDecimal.valueOf(2220L));
        customer.setRevenue(revenue);
        customer.setDirectoryId(directoryId);
        when(customerRepository.findById(any())).thenReturn(Optional.of(customer));
        // WHEN
        MonthlyConsumptionResponse response = monthlyConsumptionService.calculateMonthlyConsumption("EKW12345678", date);
        // THEN
        assertEquals(expected, response.monthlyConsumption());
    }

    @Test
    void calculateMonthlyConsumption_should_throw_CusstomerNotFoundException_when_customer_not_present() {
        // GIVEN
        when(customerRepository.findById(any())).thenReturn(Optional.empty());
        // WHEN
        assertThrows(CustomerNotFoundException.class, () ->
                monthlyConsumptionService.calculateMonthlyConsumption("EKW12345678", "2023-01"));
        verify(customerRepository).findById(any());

    }

}